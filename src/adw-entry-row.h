/*
 * Copyright (C) 2021 Maximiliano Sandoval <msandova@protonmail.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(_ADWAITA_INSIDE) && !defined(ADWAITA_COMPILATION)
#error "Only <adwaita.h> can be included directly."
#endif

#include "adw-version.h"

#include "adw-preferences-row.h"

G_BEGIN_DECLS

#define ADW_TYPE_ENTRY_ROW (adw_entry_row_get_type())

ADW_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (AdwEntryRow, adw_entry_row, ADW, ENTRY_ROW, AdwPreferencesRow)

/**
 * AdwEntryRowClass
 * @parent_class: The parent class
 */
struct _AdwEntryRowClass
{
  AdwPreferencesRowClass parent_class;
};

ADW_AVAILABLE_IN_ALL
GtkWidget *adw_entry_row_new (void) G_GNUC_WARN_UNUSED_RESULT;

ADW_AVAILABLE_IN_ALL
gboolean adw_entry_row_get_use_underline (AdwEntryRow *self);
ADW_AVAILABLE_IN_ALL
void adw_entry_row_set_use_underline     (AdwEntryRow *self,
                                          gboolean     use_underline);

ADW_AVAILABLE_IN_ALL
void adw_entry_row_add_prefix (AdwEntryRow *self,
                               GtkWidget   *widget);
ADW_AVAILABLE_IN_ALL
void adw_entry_row_add_suffix (AdwEntryRow *self,
                               GtkWidget   *widget);
ADW_AVAILABLE_IN_ALL
void adw_entry_row_remove     (AdwEntryRow *self,
                               GtkWidget   *widget);

G_END_DECLS

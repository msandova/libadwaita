/*
 * Copyright (C) 2021 Maximiliano Sandoval <msandova@protonmail.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"
#include <glib/gi18n-lib.h>

#include "adw-password-entry-row.h"

#include "adw-macros-private.h"

/**
 * AdwPasswordEntryRow:
 *
 * `AdwPasswordEntryRow` is an entry that has been tailored for entering
 * secrets inside a list.
 *
 * It does not show its contents in clear text, does not allow to copy it
 * to the clipboard, and it shows a warning when Caps Lock is engaged. If
 * the underlying platform allows it, `AdwPasswordEntryRow` will also place
 * the text in a non-pageable memory area, to avoid it being written out
 * to disk by the operating system.
 *
 * Optionally, it can offer a way to reveal the contents in clear text.
 *
 * `AdwPasswordEntryRow` provides only minimal API and should be used with
 * the [iface@Gtk.Editable] API.
 *
 * # CSS Nodes
 *
 * `AdwPasswordEntryRow` has a single CSS node with name row that carries
 * a .password style class. The text CSS node below it has a child with
 * name image and style class .caps-lock-indicator for the Caps Lock
 * icon, and possibly other children.
 *
 * Since: 1.0
 */

typedef struct
{
  GtkWidget *visibility_toggle;
  GtkWidget *caps_lock_indicator;

  GdkDevice *keyboard;
} AdwPasswordEntryRowPrivate;

G_DEFINE_TYPE_WITH_CODE (AdwPasswordEntryRow, adw_password_entry_row, ADW_TYPE_ENTRY_ROW,
                         G_ADD_PRIVATE (AdwPasswordEntryRow))

static void
caps_lock_state_changed (GdkDevice  *device,
                         GParamSpec *pspec,
                         GtkWidget   *widget)
{
  AdwPasswordEntryRow *self = ADW_PASSWORD_ENTRY_ROW (widget);
  AdwPasswordEntryRowPrivate *priv = adw_password_entry_row_get_instance_private (self);
  GtkEditable *delegate = gtk_editable_get_delegate (GTK_EDITABLE (widget));

  g_return_if_fail (GTK_IS_TEXT (delegate));

  gtk_widget_set_visible (priv->caps_lock_indicator,
                          gtk_widget_has_focus (GTK_WIDGET (delegate)) &&
                          !gtk_text_get_visibility (GTK_TEXT (delegate)) &&
                          gdk_device_get_caps_lock_state (device));
}

static void
notify_visibility_cb (AdwPasswordEntryRow *self,
                      GParamSpec *pspec,
                      GtkText *delegate)
{
  AdwPasswordEntryRowPrivate *priv = adw_password_entry_row_get_instance_private (self);

  if (gtk_text_get_visibility (GTK_TEXT (delegate))) {
    gtk_button_set_icon_name (GTK_BUTTON (priv->visibility_toggle), "eye-open-negative-filled-symbolic");
    gtk_widget_set_tooltip_text (GTK_WIDGET (priv->visibility_toggle), _("Hide Text"));
  } else {
    gtk_button_set_icon_name (GTK_BUTTON (priv->visibility_toggle), "eye-not-looking-symbolic");
    gtk_widget_set_tooltip_text (GTK_WIDGET (priv->visibility_toggle), _("Show Text"));
  }

  if (priv->keyboard)
    caps_lock_state_changed (priv->keyboard, NULL, GTK_WIDGET (self));
}

static void
notify_has_focus_cb (AdwPasswordEntryRow *self)
{
  AdwPasswordEntryRowPrivate *priv = adw_password_entry_row_get_instance_private (self);

  if (priv->keyboard)
    caps_lock_state_changed (priv->keyboard, NULL, GTK_WIDGET (self));
}

static void
adw_password_entry_row_realize (GtkWidget *widget)
{
  AdwPasswordEntryRow *self = ADW_PASSWORD_ENTRY_ROW (widget);
  AdwPasswordEntryRowPrivate *priv = adw_password_entry_row_get_instance_private (self);
  GdkSeat *seat;

  GTK_WIDGET_CLASS (adw_password_entry_row_parent_class)->realize (widget);

  seat = gdk_display_get_default_seat (gtk_widget_get_display (widget));
  if (seat)
    priv->keyboard = gdk_seat_get_keyboard (seat);

  if (priv->keyboard)
    {
      g_signal_connect (priv->keyboard, "notify::caps-lock-state",
                        G_CALLBACK (caps_lock_state_changed), self);
      caps_lock_state_changed (priv->keyboard, NULL, widget);
    }
}

static void
adw_password_entry_row_dispose (GObject *object)
{
  AdwPasswordEntryRow *self = ADW_PASSWORD_ENTRY_ROW (object);
  AdwPasswordEntryRowPrivate *priv = adw_password_entry_row_get_instance_private (self);

  if (priv->keyboard)
    g_signal_handlers_disconnect_by_func (priv->keyboard, caps_lock_state_changed, self);

  G_OBJECT_CLASS (adw_password_entry_row_parent_class)->dispose (object);
}

static void
adw_password_entry_row_class_init (AdwPasswordEntryRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = adw_password_entry_row_dispose;

  widget_class->realize = adw_password_entry_row_realize;

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/gnome/Adwaita/ui/adw-password-entry-row.ui");

  gtk_widget_class_bind_template_child_private (widget_class, AdwPasswordEntryRow, visibility_toggle);
  gtk_widget_class_bind_template_child_private (widget_class, AdwPasswordEntryRow, caps_lock_indicator);
}

static void
adw_password_entry_row_init (AdwPasswordEntryRow *self)
{
  AdwPasswordEntryRowPrivate *priv = adw_password_entry_row_get_instance_private (self);
  GtkEntryBuffer *buffer = gtk_password_entry_buffer_new ();
  GtkEditable *delegate = gtk_editable_get_delegate (GTK_EDITABLE (self));

  g_return_if_fail (GTK_IS_TEXT (delegate));

  gtk_widget_init_template (GTK_WIDGET (self));

  gtk_text_set_visibility (GTK_TEXT (delegate), FALSE);

  gtk_text_set_buffer (GTK_TEXT (delegate), buffer);

  g_signal_connect_swapped (delegate, "notify::has-focus", G_CALLBACK (notify_has_focus_cb), self);
  g_signal_connect_swapped (delegate, "notify::visibility", G_CALLBACK (notify_visibility_cb), self);
  g_object_bind_property (priv->visibility_toggle, "active",
                          delegate, "visibility",
                          G_BINDING_SYNC_CREATE);
}

/**
 * adw_password_entry_row_new:
 *
 * Creates a new `AdwPasswordEntryRow`.
 *
 * Returns: the newly created `AdwPasswordEntryRow`
 *
 * Since: 1.0
 */
GtkWidget *
adw_password_entry_row_new (void)
{
  return g_object_new (ADW_TYPE_PASSWORD_ENTRY_ROW, NULL);
}

/*
 * Copyright (C) 2021 Maximiliano Sandoval <msandova@protonmail.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(_ADWAITA_INSIDE) && !defined(ADWAITA_COMPILATION)
#error "Only <adwaita.h> can be included directly."
#endif

#include "adw-version.h"

#include "adw-entry-row.h"

G_BEGIN_DECLS

#define ADW_TYPE_PASSWORD_ENTRY_ROW (adw_password_entry_row_get_type())

ADW_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (AdwPasswordEntryRow, adw_password_entry_row, ADW, PASSWORD_ENTRY_ROW, AdwEntryRow)

/**
 * AdwPasswordEntryRowClass
 * @parent_class: The parent class
 */
struct _AdwPasswordEntryRowClass
{
  AdwEntryRowClass parent_class;
};

ADW_AVAILABLE_IN_ALL
GtkWidget *adw_password_entry_row_new (void) G_GNUC_WARN_UNUSED_RESULT;

G_END_DECLS

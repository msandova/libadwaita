/*
 * Copyright (C) 2021 Maximiliano Sandoval <msandova@protonmail.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"
#include "adw-entry-row.h"

#include "adw-focus-private.h"
#include "adw-macros-private.h"

/**
 * AdwEntryRow:
 *
 * A [class@Gtk.ListBoxRow] used to present an entry inside lists.
 * An edit icon is shown to indicate whether the underlying [class@Gtk.Text] has focus
 * and the widget is [property@Gtk.Editable:editable].
 *
 * The `AdwEntryRow` widget can have a [property@Adw.PreferencesRow:title]
 * and [property@Gtk.Editable:text]. The row
 * can receive additional widgets at its end, or prefix widgets at its start.
 *
 * `AdwEntryRow` text does not have API of its own, but it
 * implements the [iface@Gtk.Editable] interface
 * and partially exposes the internal [class@Gtk.Text] API.
 *
 * ## AdwEntryRow as GtkBuildable
 *
 * The `AdwEntryRow` implementation of the [iface@Gtk.Buildable] interface
 * supports adding children at its end by specifying “suffix” or omitting the
 * “type” attribute of a <child> element.
 *
 * It also supports adding a children as a prefix widget by specifying “prefix” as
 * the “type” attribute of a <child> element.
 *
 * # CSS nodes
 *
 * `AdwEntryRow` has a main CSS node with name `row` and the `.entry` style class.
 *
 * Since: 1.0
 */

typedef struct
{
  GtkWidget *edit_icon;
  GtkWidget *header;
  GtkWidget *text;
  GtkLabel *title;
  GtkStack *stack;
  GtkBox *title_box;
  GtkBox *suffixes;
  GtkBox *prefixes;

  gboolean use_underline;
  GtkWidget *previous_parent;

} AdwEntryRowPrivate;

static void adw_entry_row_editable_init (GtkEditableInterface *iface);
static void adw_entry_row_buildable_init (GtkBuildableIface *iface);

G_DEFINE_TYPE_WITH_CODE (AdwEntryRow, adw_entry_row, ADW_TYPE_PREFERENCES_ROW,
                         G_ADD_PRIVATE (AdwEntryRow)
                         G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE, adw_entry_row_buildable_init)
                         G_IMPLEMENT_INTERFACE (GTK_TYPE_EDITABLE, adw_entry_row_editable_init))

static GtkBuildableIface *parent_buildable_iface;

enum {
  PROP_0,
  PROP_USE_UNDERLINE,
  PROP_LAST_PROP,
};

static GParamSpec *props[PROP_LAST_PROP];

/**
 * buffer_is_not_empty:
 * @self: a `AdwEntryRow`
 *
 * Checks if the buffer used by the editable delegate
 * is empty. Due to the memory security required by `AdwPasswordEntryRow`
 * we cannot invoke [property@Gtk.Editable:text].
 */
static gboolean
buffer_is_not_empty (AdwEntryRow *self)
{
  GtkEditable *text;
  GtkEntryBuffer *buffer;
  guint length;

  text = gtk_editable_get_delegate (GTK_EDITABLE (self));
  buffer = gtk_text_get_buffer (GTK_TEXT (text));
  length = gtk_entry_buffer_get_length (buffer);
  return (length > 0);
}

/*< private >
 * start_editing:
 * @self: a `AdwEntryRow`
 *
 * Switches the entry into “editing mode”, this takes care of
 * animations and hiding the edit icon.
 *
 * Since: 1.0
 */
static void
start_editing (AdwEntryRow *self)
{
  AdwEntryRowPrivate *priv = adw_entry_row_get_instance_private (self);

  g_return_if_fail (ADW_IS_ENTRY_ROW (self));

  gtk_stack_set_visible_child_name (priv->stack, "title-and-subtitle");

  gtk_widget_hide (priv->edit_icon);
}

/*< private >
 * stop_editing:
 * @self: a `AdwEntryRow`
 *
 * Switches the entry out of “editing mode”.
 *
 * Since: 1.0
 */
static void
stop_editing (AdwEntryRow *self)
{
  AdwEntryRowPrivate *priv = adw_entry_row_get_instance_private (self);

  g_return_if_fail (ADW_IS_ENTRY_ROW (self));

  if (buffer_is_not_empty (self))
    gtk_stack_set_visible_child_name (priv->stack, "title-and-subtitle");
  else
    gtk_stack_set_visible_child_name (priv->stack, "title-only");

  gtk_widget_set_visible (priv->edit_icon,
                         gtk_editable_get_editable (GTK_EDITABLE (priv->text)));
}

static gboolean
string_is_not_empty (GBinding     *binding,
                     const GValue *from_value,
                     GValue       *to_value,
                     gpointer      user_data)
{
  const char *string = g_value_get_string (from_value);

  g_value_set_boolean (to_value, string && string[0]);

  return TRUE;
}

static void
row_activated_cb (AdwEntryRow   *self,
                  GtkListBoxRow *row)
{
  if ((GtkListBoxRow *) self == row) {
    start_editing (self);
    // TODO Revisit this grab.
    gtk_widget_grab_focus (GTK_WIDGET (self));
  }
}

static void
parent_cb (AdwEntryRow *self)
{
  AdwEntryRowPrivate *priv = adw_entry_row_get_instance_private (self);
  GtkWidget *parent = gtk_widget_get_parent (GTK_WIDGET (self));

  if (priv->previous_parent != NULL) {
    g_signal_handlers_disconnect_by_func (priv->previous_parent, G_CALLBACK (row_activated_cb), self);
    priv->previous_parent = NULL;
  }

  if (parent == NULL || !GTK_IS_LIST_BOX (parent))
    return;

  priv->previous_parent = parent;
  g_signal_connect_swapped (parent, "row-activated", G_CALLBACK (row_activated_cb), self);
}

static void
controller_leave_cb (AdwEntryRow *self)
{
  stop_editing (self);
}

static void
controller_enter_cb (AdwEntryRow *self)
{
  start_editing (self);
}

static void
text_activate_cb (AdwEntryRow *self)
{
  stop_editing (self);
  adw_widget_grab_focus_self (GTK_WIDGET (self));
}

static void
notify_text_cb (AdwEntryRow *self)
{
  /* Sync the entry text to the label, unless we are editing.
   *
   * This is necessary to catch APIs like `gtk_editable_insert_text`,
   * which don't go through the text property.
   */
  AdwEntryRowPrivate *priv = adw_entry_row_get_instance_private (self);

  if (!gtk_widget_has_focus (priv->text))
    {
      if (buffer_is_not_empty (self))
        gtk_stack_set_visible_child_name (priv->stack, "title-and-subtitle");
      else
        gtk_stack_set_visible_child_name (priv->stack, "title-only");
    }
}

static void
adw_entry_row_get_property (GObject     *object,
                            guint        prop_id,
                            GValue      *value,
                            GParamSpec  *pspec)
{
  AdwEntryRow *self = ADW_ENTRY_ROW (object);

  if (gtk_editable_delegate_get_property (object, prop_id, value, pspec))
    return;

  switch (prop_id) {
  case PROP_USE_UNDERLINE:
    g_value_set_boolean (value, adw_entry_row_get_use_underline (self));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
adw_entry_row_set_property (GObject       *object,
                            guint          prop_id,
                            const GValue  *value,
                            GParamSpec    *pspec)
{
  AdwEntryRow *self = ADW_ENTRY_ROW (object);
  AdwEntryRowPrivate *priv = adw_entry_row_get_instance_private (self);

  if (gtk_editable_delegate_set_property (object, prop_id, value, pspec))
  {
    switch (prop_id) {
    case PROP_LAST_PROP + GTK_EDITABLE_PROP_EDITABLE: {
      gboolean editable = g_value_get_boolean (value);
      gboolean has_focus = gtk_widget_has_focus (priv->text);

      if (!editable)
        stop_editing (self);

      gtk_widget_set_visible (priv->edit_icon, editable && !has_focus);
    } break;

    default:;
    }
    return;
  }

  switch (prop_id) {
  case PROP_USE_UNDERLINE:
    adw_entry_row_set_use_underline (self, g_value_get_boolean (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static gboolean
adw_entry_row_grab_focus (GtkWidget *self)
{
  AdwEntryRowPrivate *priv = adw_entry_row_get_instance_private (ADW_ENTRY_ROW (self));

  if (gtk_editable_get_editable (GTK_EDITABLE (priv->text)))
    return adw_widget_grab_focus_self (priv->text);
  else
    return adw_widget_grab_focus_self (self);
}

static void
adw_entry_row_dispose (GObject *object)
{
  AdwEntryRow *self = ADW_ENTRY_ROW (object);
  AdwEntryRowPrivate *priv = adw_entry_row_get_instance_private (self);

  if (priv->previous_parent != NULL) {
    g_signal_handlers_disconnect_by_func (priv->previous_parent, G_CALLBACK (row_activated_cb), self);
    priv->previous_parent = NULL;
  }

  if (priv->text)
      gtk_editable_finish_delegate (GTK_EDITABLE (self));

  G_OBJECT_CLASS (adw_entry_row_parent_class)->dispose (object);
}

static void
adw_entry_row_class_init (AdwEntryRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->get_property = adw_entry_row_get_property;
  object_class->set_property = adw_entry_row_set_property;
  object_class->dispose = adw_entry_row_dispose;

  widget_class->grab_focus = adw_entry_row_grab_focus;

  /**
   * AdwEntryRow:use-underline: (attributes org.gtk.Property.get=adw_entry_row_get_use_underline org.gtk.Property.set=adw_entry_row_set_use_underline)
   *
   * Whether underlines in title are interpreted as mnemonics.
   *
   * Since: 1.0
   */
  props[PROP_USE_UNDERLINE] =
    g_param_spec_boolean ("use-underline",
                          "Use underline",
                          "Whether underlines in title or subtitle are interpreted as mnemonics",
                          FALSE,
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);


  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);

  gtk_editable_install_properties (object_class, PROP_LAST_PROP);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/gnome/Adwaita/ui/adw-entry-row.ui");
  gtk_widget_class_bind_template_child_private (widget_class, AdwEntryRow, edit_icon);
  gtk_widget_class_bind_template_child_private (widget_class, AdwEntryRow, header);
  gtk_widget_class_bind_template_child_private (widget_class, AdwEntryRow, prefixes);
  gtk_widget_class_bind_template_child_private (widget_class, AdwEntryRow, stack);
  gtk_widget_class_bind_template_child_private (widget_class, AdwEntryRow, suffixes);
  gtk_widget_class_bind_template_child_private (widget_class, AdwEntryRow, text);
  gtk_widget_class_bind_template_child_private (widget_class, AdwEntryRow, title);
  gtk_widget_class_bind_template_child_private (widget_class, AdwEntryRow, title_box);

  gtk_widget_class_bind_template_callback (widget_class, controller_enter_cb);
  gtk_widget_class_bind_template_callback (widget_class, controller_leave_cb);
}

static void
adw_entry_row_init (AdwEntryRow *self)
{
  AdwEntryRowPrivate *priv = adw_entry_row_get_instance_private (self);

  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_editable_init_delegate (GTK_EDITABLE (self));

  g_object_bind_property_full (self, "title", priv->title, "visible", G_BINDING_SYNC_CREATE,
                               string_is_not_empty, NULL, NULL, NULL);

  g_signal_connect (self, "notify::parent", G_CALLBACK (parent_cb), NULL);
  g_signal_connect_swapped (priv->text, "activate", G_CALLBACK (text_activate_cb), self);
  g_signal_connect_swapped (priv->text, "notify::text", G_CALLBACK (notify_text_cb), self);
}

static void
adw_entry_row_buildable_add_child (GtkBuildable *buildable,
                                   GtkBuilder   *builder,
                                   GObject      *child,
                                   const char   *type)
{
  AdwEntryRow *self = ADW_ENTRY_ROW (buildable);
  AdwEntryRowPrivate *priv = adw_entry_row_get_instance_private (self);

  if (!priv->header)
    parent_buildable_iface->add_child (buildable, builder, child, type);
  else if (g_strcmp0 (type, "prefix") == 0)
    adw_entry_row_add_prefix (self, GTK_WIDGET (child));
  else if (g_strcmp0 (type, "suffix") == 0)
    adw_entry_row_add_suffix (self, GTK_WIDGET (child));
  else if (!type && GTK_IS_WIDGET (child))
    adw_entry_row_add_suffix (self, GTK_WIDGET (child));
  else
    parent_buildable_iface->add_child (buildable, builder, child, type);
}

static void
adw_entry_row_buildable_init (GtkBuildableIface *iface)
{
  parent_buildable_iface = g_type_interface_peek_parent (iface);
  iface->add_child = adw_entry_row_buildable_add_child;
}

static GtkEditable *
adw_entry_row_get_delegate (GtkEditable *editable)
{
  AdwEntryRowPrivate *priv;

  priv = adw_entry_row_get_instance_private (ADW_ENTRY_ROW (editable));

  return GTK_EDITABLE (priv->text);
}

void
adw_entry_row_editable_init (GtkEditableInterface *iface)
{
  iface->get_delegate = adw_entry_row_get_delegate;
}

/**
 * adw_entry_row_new:
 *
 * Creates a new `AdwEntryRow`.
 *
 * Returns: the newly created `AdwEntryRow`
 *
 * Since: 1.0
 */
GtkWidget *
adw_entry_row_new (void)
{
  return g_object_new (ADW_TYPE_ENTRY_ROW, NULL);
}

/**
 * adw_entry_row_get_use_underline: (attributes org.gtk.Method.get_property=use-underline)
 * @self: a `AdwEntryRow`
 *
 * Gets whether underlines in [property@Adw.EntryRow:title] are interpreted as mnemonics.
 *
 * Returns: `TRUE` if underlines are interpreted as mnemonics
 *
 * Since: 1.0
 */
gboolean
adw_entry_row_get_use_underline (AdwEntryRow *self)
{
  AdwEntryRowPrivate *priv;

  g_return_val_if_fail (ADW_IS_ENTRY_ROW (self), FALSE);

  priv = adw_entry_row_get_instance_private (self);

  return priv->use_underline;
}

/**
 * adw_entry_row_set_use_underline: (attributes org.gtk.Method.set_property=use-underline)
 * @self: a `AdwEntryRow`
 * @use_underline: whether underlines in [property@Adw.EntryRow:title] are interpreted as mnemonics
 *
 * Sets whether underlines are interpreted as mnemonics.
 *
 * Since: 1.0
 */
void
adw_entry_row_set_use_underline (AdwEntryRow *self,
                                 gboolean     use_underline)
{
  AdwEntryRowPrivate *priv;

  g_return_if_fail (ADW_IS_ENTRY_ROW (self));

  priv = adw_entry_row_get_instance_private (self);

  use_underline = !!use_underline;

  if (priv->use_underline == use_underline)
    return;

  priv->use_underline = use_underline;
  adw_preferences_row_set_use_underline (ADW_PREFERENCES_ROW (self), priv->use_underline);
  gtk_label_set_use_underline (priv->title, priv->use_underline);
  gtk_label_set_mnemonic_widget (priv->title, GTK_WIDGET (self));

  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_USE_UNDERLINE]);
}

/**
 * adw_entry_row_add_prefix:
 * @self: a `AdwEntryRow`
 * @widget: a widget
 *
 * Adds a prefix widget to @self.
 *
 * Since: 1.0
 */
void
adw_entry_row_add_prefix (AdwEntryRow *self,
                          GtkWidget   *widget)
{
  AdwEntryRowPrivate *priv;

  g_return_if_fail (ADW_IS_ENTRY_ROW (self));
  g_return_if_fail (GTK_IS_WIDGET (widget));

  priv = adw_entry_row_get_instance_private (self);

  gtk_box_prepend (priv->prefixes, widget);
  gtk_widget_show (GTK_WIDGET (priv->prefixes));
}

/**
 * adw_entry_row_add_suffix:
 * @self: a `AdwEntryRow`
 * @widget: a widget
 *
 * Adds a suffix widget to @self.
 *
 * Since: 1.0
 */
void
adw_entry_row_add_suffix (AdwEntryRow *self,
                          GtkWidget    *widget)
{
  AdwEntryRowPrivate *priv;

  g_return_if_fail (ADW_IS_ENTRY_ROW (self));
  g_return_if_fail (GTK_IS_WIDGET (widget));

  priv = adw_entry_row_get_instance_private (self);

  gtk_box_append (priv->suffixes, widget);
  gtk_widget_show (GTK_WIDGET (priv->suffixes));
}

/**
 * adw_entry_row_remove:
 * @self: a `AdwEntryRow`
 * @widget: the child to be removed
 *
 * Removes a child from @self.
 *
 * Since: 1.0
 */
void
adw_entry_row_remove (AdwEntryRow *self,
                      GtkWidget   *child)
{
  AdwEntryRowPrivate *priv;
  GtkWidget *parent;

  g_return_if_fail (ADW_IS_ENTRY_ROW (self));
  g_return_if_fail (GTK_IS_WIDGET (child));

  priv = adw_entry_row_get_instance_private (self);

  parent = gtk_widget_get_parent (child);

  if (parent == GTK_WIDGET (priv->prefixes))
    gtk_box_remove (priv->prefixes, child);
  else if (parent == GTK_WIDGET (priv->suffixes))
    gtk_box_remove (priv->suffixes, child);
  else
    ADW_CRITICAL_CANNOT_REMOVE_CHILD (self, child);
}
